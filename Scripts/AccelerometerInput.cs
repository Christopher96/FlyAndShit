using System;
using UnityEngine;
using System.Collections;

public class AccelerometerInput : MonoBehaviour {

	public float maxRotation;
	public float maxOffset;
	public float maxAcceleration;
	public float flySpeed;
	public float rotSpeed;

	float zAcceleration = 0f;
    bool tiltSet = false;
    float zOffset = 0;
	
	// Update is called once per frame
    void Update() {
        zAcceleration = Input.acceleration.z;

        if( !tiltSet && GameUpdate.start ){
            float maxOffset = 1 - maxAcceleration;
            float minOffset = maxAcceleration - 1;

            if ( zAcceleration >  maxOffset ){
                zOffset = maxOffset;
            } else if( zAcceleration < minOffset ){
                zOffset = minOffset;
            } else {
                zOffset = zAcceleration;
            }

            tiltSet = true;
        }

        zAcceleration = zAcceleration - zOffset;

        if( zAcceleration > maxAcceleration ) 
            zAcceleration = maxAcceleration;
        else if( zAcceleration < -maxAcceleration ) 
            zAcceleration = -maxAcceleration;
    }

    void FixedUpdate(){

        if ( !GameUpdate.start ) return;

		// Y POSITION OF BIRD
		Vector3 newPos = Vector3.zero;
		newPos.y = zAcceleration;
    	transform.Translate( newPos * flySpeed * Time.deltaTime, Space.World );

        // AUTOMATICALLY ROTATE TO ZERO
        Quaternion target = Quaternion.Euler(0,0,0);

    	// CHECK IF BIRD IS INSIDE BORDERS
    	Vector3 currPos = transform.position;
    	if( currPos.y >= maxOffset ){
    		currPos.y = maxOffset;
    		transform.position = currPos;

    	} else if ( currPos.y <= -maxOffset ) {
    		currPos.y = -maxOffset;
    		transform.position = currPos;

    	} else {
    		target.z = zAcceleration;
    	}

    	float step = rotSpeed * Time.deltaTime;
        transform.rotation = Quaternion.RotateTowards(transform.rotation, target, step);
    }
}
