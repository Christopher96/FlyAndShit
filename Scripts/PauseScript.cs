﻿using UnityEngine;
using System.Collections;

public class PauseScript : MonoBehaviour {

	public float fadeInTime;

	CanvasRenderer rend;
	float timer = 0;

	void Start () {
		rend = GetComponent<CanvasRenderer>();
		setGUIalpha( 0 );
	}
	
	// Update is called once per frame
	void Update () {
		if( GameUpdate.dead ){
			GameUpdate.start = false;
			if( rend.GetAlpha() > 1 ){

				foreach( Touch touch in Input.touches ){
					GameUpdate.dead = false;
		    		Application.LoadLevel( Application.loadedLevel );
		    	}
			} else {
				timer += Time.deltaTime;
				setGUIalpha( timer / fadeInTime );
			}
		}
	}

	void setGUIalpha( float alpha ){
		rend.SetAlpha( alpha );

		foreach ( Transform child in transform ){
			CanvasRenderer childRend = child.GetComponent<CanvasRenderer>();
			childRend.SetAlpha( alpha );
		}
	}
}
