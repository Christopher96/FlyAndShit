﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CountdownScript : MonoBehaviour {

	float count;
	Text countText;
	// Use this for initialization
	void Start () {
		count = GameUpdate.startDelay;
		countText = GetComponent<Text>();

		countText.text = count.ToString();
		countText.enabled = false;
		StartCoroutine( countDown() );
	}

	void Update(){
		if( Time.time > 0 ) countText.enabled = true;
	}
	
    IEnumerator countDown() {
        yield return new WaitForSeconds( 1f );
        GetComponent<AudioSource>().Play();
        count -= 1f;
        if( count < 1 ) countText.text = "";
        else{
        	countText.text = count.ToString();
       	 	StartCoroutine( countDown() );
        }
    }
}
