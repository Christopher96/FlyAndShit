﻿using UnityEngine;
using System.Collections;

public class BuildingBehaviour : MonoBehaviour {

	public GameObject bottom;
	public GameObject middle;
	public GameObject top;

	public int maxLevels;

	// Use this for initialization
	void Start () {

		Vector3 bottomSize = bottom.GetComponent<Renderer>().bounds.extents;
		Vector3 middleSize = middle.GetComponent<Renderer>().bounds.extents;
		Vector3 topSize    =    top.GetComponent<Renderer>().bounds.extents;

		Vector3 pos = transform.position;
		Quaternion rot = transform.rotation;

		// BOTTOM LEVEL
		InstatiateAndParent( bottom, pos, rot );

		int levels = Random.Range( 1, maxLevels );

		// MIDDLE LEVEL
		pos.y += bottomSize.y;

		for( int i=0; i<levels; i++ ){
			pos.y += middleSize.y;
			InstatiateAndParent( middle, pos, rot );
			pos.y += middleSize.y;
		}

		pos.y += topSize.y;
		// TOP LEVEL
		InstatiateAndParent( top, pos, rot );
	}

	void InstatiateAndParent( GameObject obj, Vector3 pos, Quaternion rot ){
		GameObject clone = Instantiate( obj, pos, rot ) as GameObject;
		clone.transform.SetParent( transform );
	}

}
