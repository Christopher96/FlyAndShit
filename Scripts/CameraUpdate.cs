﻿using UnityEngine;
using System.Collections;

public class CameraUpdate : MonoBehaviour {

	public float scrollSpeed;

	void FixedUpdate(){

		Vector3 newPosition = transform.position;
		newPosition.x += scrollSpeed;

		transform.position = newPosition;
	}


}
