﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerUpdate : MonoBehaviour {

	public int scoreApplier;
	public float scoreDelay;
	float scoreTimer;

	public float shitAmount;
	public float shitDelay;
	float shitTimer;

	public GameObject shit;
	public GameObject splatter;

	public Scrollbar statusbar;

	public bool godMode = false;

	public Vector2 fingerPosition;

	void Start(){
		shitTimer = shitDelay;
		StartCoroutine( startTimer() );
	}

	IEnumerator startTimer() {
		yield return new WaitForSeconds( GameUpdate.startDelay );
		GameUpdate.start = true;
	}
	
	// Update is called once per frame
	void Update () {

		foreach( Touch touch in Input.touches ){
    		fingerPosition = touch.position;
    		if ( touch.phase == TouchPhase.Began && shitTimer >= ( shitDelay / shitAmount ) && GameUpdate.start && !GameUpdate.dead ){
    			Instantiate(shit, transform.position, transform.rotation);
    			shitTimer -= 0.7f;
    		}
    	}
	}

	void FixedUpdate(){

		scoreTimer += Time.deltaTime;
		if( GameUpdate.start && scoreTimer > scoreDelay ){
			GameUpdate.addScore( scoreApplier );
			scoreTimer = 0;
		}

		if( shitTimer < shitDelay ) shitTimer += Time.deltaTime;
		else shitTimer = shitDelay;
		float percentage = shitTimer / shitDelay;
		statusbar.size = percentage;
	}


	void OnTriggerEnter2D( Collider2D collider ){
		if (!godMode){
			Destroy( gameObject );
			Instantiate( splatter, transform.position, transform.rotation );
			GameUpdate.dead = true;
		}
	}
}
