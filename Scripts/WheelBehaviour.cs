﻿using UnityEngine;
using System.Collections;

public class WheelBehaviour : MonoBehaviour {
	public float rotationSpeed;

	void FixedUpdate () {
		transform.Rotate( Vector3.forward * Time.deltaTime * rotationSpeed );
	}
}
