﻿using UnityEngine;
using System.Collections;

public class PointScript : MonoBehaviour {

	float fadeTimer;
	CanvasRenderer rend;
	// Use this for initialization
	void Start () {
		rend = GetComponent<CanvasRenderer>();

		GameObject canvas = GameObject.Find("Canvas");
		transform.SetParent( canvas.transform, false );
	}

	// Update is called once per frame
	void FixedUpdate () {
		fadeTimer += Time.deltaTime;

		if ( rend.GetAlpha() > 0.3 ){
			rend.SetAlpha( 1f / (fadeTimer*3) );
		} else {
			Destroy( this.gameObject );
		}

		Vector3 pos = transform.position;
		pos.y += ( 0.008f / fadeTimer );
		transform.position = pos;
	}
}
