﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShitBehaviour : MonoBehaviour {
	public float smearOffset;
	public float initForce;

	public GameObject shitsmear;
	public GameObject splatter;

	public GameObject point;

	void Start() {
		GetComponent<Rigidbody2D>().AddForce( transform.up * -initForce );


	}
	
	void OnTriggerEnter2D( Collider2D collider ){
		if( collider.tag.Equals( "Obstacle" ) ){
			print( collider );
			Destroy( gameObject );
			
			Vector3 pos = transform.position;
			pos.y -= smearOffset;

			GameObject splatterclone = Instantiate( splatter, transform.position, transform.rotation ) as GameObject;
			GameObject smearclone = Instantiate( shitsmear, pos, transform.rotation ) as GameObject;
			smearclone.transform.parent = splatterclone.transform.parent = collider.transform;

			ObstacleScript script = collider.GetComponent<ObstacleScript>();

			if( script != null ){
				GameObject pointclone = Instantiate( point ) as GameObject;
				Vector3 relativePos = Camera.main.WorldToScreenPoint( transform.position );
				relativePos.y += 5;
				pointclone.transform.position = relativePos;
				pointclone.GetComponent<Text>().text = script.scoreApplier.ToString();
			}
		}
	}
}
