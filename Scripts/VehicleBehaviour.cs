using UnityEngine;
using System.Collections;

public class VehicleBehaviour : MonoBehaviour {

	public float speedBoost;

	public Sprite crashSprite;

	private SpriteRenderer spriteRenderer;

	void Start() {
	    spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
	}

	void Update(){
		Vector3 pos = transform.position;
		pos.x -= speedBoost;
		transform.position = pos;
	}
	
	void OnTriggerEnter2D( Collider2D collider ){
		if( collider.name.Equals("seagull") ) {
			spriteRenderer.sprite = crashSprite;
		}
	}

}
