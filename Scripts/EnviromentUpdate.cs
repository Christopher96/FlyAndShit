﻿using UnityEngine;
using System.Collections;
using System.IO;

public class EnviromentUpdate : MonoBehaviour {

	bool firstObstacle = true;
	
	public Transform fixedSpawn;

	public Transform houseSpawn;
	private Vector3 houseSpawnPos;

	public GameObject[] houses;

	public GameObject[][] obstacleLists;
	public GameObject[] vehicles;
	public GameObject[] middle;
	public GameObject[] top;

	public GameObject road;

	void Start(){

		obstacleLists = new GameObject[][]{ vehicles, middle, top };

		houseSpawnPos = houseSpawn.position;

		for( int i=0; i < 4; i++ ){
			spawnBuilding();
		}
	}

	void Update(){
		if ( GameUpdate.start && firstObstacle ){
			spawnObstacle();
			firstObstacle = false;
		}
	}

	void OnTriggerExit2D( Collider2D collider ){
		Destroy( collider.gameObject );

		if( collider.name.Contains( "house" ) ){
			spawnBuilding();
		}

	}

	void OnTriggerEnter2D( Collider2D collider ){
		
		if ( collider.name.Contains( "road" ) ){
			Vector3 size = collider.gameObject.GetComponent<Collider2D>().bounds.extents;
			fixedSpawnInstantiation( road, size.x );
		} else if ( collider.tag.Equals( "Obstacle" ) && GameUpdate.start ){
			spawnObstacle();
		}
	}

	void spawnBuilding(){
		int id = Random.Range( 0, houses.Length );
		GameObject house = houses[ id ];
		Vector3 houseSize = house.GetComponent<BuildingBehaviour>().bottom.GetComponent<Renderer>().bounds.size;
		Vector3 housePos = house.transform.position;

		houseSpawnPos.x += houseSize.x;
		houseSpawnPos.y = housePos.y;
		houseSpawnPos.z = housePos.z;
		houseSpawn.position = houseSpawnPos;

		Instantiate( house, houseSpawnPos, Quaternion.identity );
	}

	void spawnObstacle(){
		GameObject[] list = obstacleLists[ Random.Range( 0, obstacleLists.Length ) ];
		GameObject obstacle = list[ Random.Range( 0, list.Length ) ];

		fixedSpawnInstantiation( obstacle, 0 );
	}

	void fixedSpawnInstantiation( GameObject obj, float xAdd ){
		Vector3 pos = obj.transform.position;
		pos.x = fixedSpawn.transform.position.x + xAdd;

		Instantiate( obj, pos, Quaternion.identity );
	}
}
