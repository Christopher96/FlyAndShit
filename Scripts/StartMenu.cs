﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StartMenu : MonoBehaviour {

	// Use this for initialization
	public void StartGame(){
		if( !GameUpdate.dead ) return;
		GameUpdate.dead = false;
		Time.timeScale = 1;
		Destroy( this.gameObject ); 
	}

	void Start () {
		Time.timeScale = 0;
	}
	
}
