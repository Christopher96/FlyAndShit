﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameUpdate : MonoBehaviour {

	public GameObject[] PlayerUIElements;

	public static bool start = false;
	public static bool dead = true;
	public static float startDelay = 3;
	public static int score;
	public static int highscore;

	void Start (){
		score = 0;
		highscore = PlayerPrefs.GetInt( "highscore", 0 );
	}
	
	// Update is called once per frame
	void Update () {

		updateTextByTag( score.ToString(), "Score" );
		updateTextByTag( highscore.ToString(), "Highscore" );

		
		foreach( GameObject UIElement in PlayerUIElements ){
			UIElement.SetActive( start );
		}
	}

	public static void addScore( int scoreApplier ){
		score += scoreApplier;

 		if( score > highscore ){
			PlayerPrefs.SetInt( "highscore", highscore );
			highscore = score;
		}
	}

	void updateTextByTag( string text, string tag ){
		foreach ( GameObject textObject in GameObject.FindGameObjectsWithTag( tag ) ){
			textObject.GetComponent<Text>().text = text;
		}
	}
}
